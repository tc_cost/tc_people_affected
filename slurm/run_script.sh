#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=tce_run_script
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tce_people_affected/log
#SBATCH --output=tce_run_script-%A_%a.out
#SBATCH --error=tce_run_script-%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

# add --array=1-$(cat $1 | wc -l) to the sbatch call

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniforge3
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate climada_env

cd /home/tovogt/code/tce_people_affected
SCRIPT_FILE=$1
shift
ARGS=$(sed -n "${SLURM_ARRAY_TASK_ID}p" $1)
echo $SCRIPT_FILE $ARGS > log/tce_run_script-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.args
python -u scripts/$SCRIPT_FILE $ARGS
