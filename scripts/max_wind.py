
import logging
import pathlib
import sys

from numba import njit
import numpy as np
import pandas as pd
import xarray as xr

import climada.util.coordinates as u_coord
from climada_petals.hazard.emulator import const


LOGGER = logging.getLogger('tce_people_affected')
LOGGER.setLevel(logging.DEBUG)
LOGGER.propagate = False
if LOGGER.hasHandlers():
    for handler in LOGGER.handlers:
        LOGGER.removeHandler(handler)
FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
CONSOLE = logging.StreamHandler(stream=sys.stdout)
CONSOLE.setFormatter(FORMATTER)
LOGGER.addHandler(CONSOLE)


INPUT_DIR = pathlib.Path("input")
OUTPUT_DIR = pathlib.Path("output")

POP_DIR = INPUT_DIR / "pop"
NAT_FILE = lambda basin: POP_DIR / f"natids_{basin}.nc"

WIND_DIR = lambda suffix: INPUT_DIR / f"wind_{suffix}"
WIND_FILE_OLD = lambda gcm, rcp, basin: (
    f"TC-hazard_random_realizations-100_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_baseyear-2015_GMT-time_LTE.nc")
WIND_FILE_NEW = lambda gcm, rcp, basin: f"{gcm}_{rcp}_{basin}_1980_2100_100.nc"
WIND_FILE_SCENARIO = lambda gcm, rcp, basin, scenario: (
    f"{gcm}_{rcp}_{basin}__{scenario}_1980-2100_100.nc"
)
WIND_FILE = lambda gcm, rcp, basin, suffix, scenario: WIND_DIR(suffix).joinpath(
    WIND_FILE_SCENARIO(gcm, rcp, basin, scenario) if scenario is not None
    else WIND_FILE_NEW(gcm, rcp, basin) if suffix != "old"
    else WIND_FILE_OLD(gcm, rcp, basin)
)

OUTPUT = lambda gcm, rcp, basin, suffix: OUTPUT_DIR.joinpath(
    f"TC-max_wind_by_event_and_country_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_{suffix}.csv")


@njit
def maximum_at(out, bins, weight):
    for k in range(weight.shape[0]):
        for i in range(weight.shape[1]):
            # exclude 0-bin because it's for ocean
            if bins[i] > 0 and out[k, bins[i]] < weight[k, i]:
                out[k, bins[i]] = weight[k, i]
    return out


def main(gcm, rcp, basin, wind_input, scenario):
    natids = xr.open_dataset(NAT_FILE(basin)).natids.values
    natids[natids <= 0] = 0
    ds = xr.open_dataset(WIND_FILE(gcm, rcp, basin, wind_input, scenario))
    ds['event_id'] = ('event', np.arange(ds.event.size))
    tcwind = 'tcwind' if 'tcwind' in ds.variables else 'wind'
    natids_uniq, natids_inverse = np.unique(natids, return_inverse=True)
    years_uniq = np.unique(ds.time.dt.year)

    suffix = wind_input if scenario is None else f"{scenario}_{wind_input}"
    outpath = OUTPUT(gcm, rcp, basin, suffix)
    total = []

    ymin, ymax = years_uniq[0], years_uniq[-1]
    if outpath.exists():
        LOGGER.info(f"Load {outpath} ...")
        total.append(pd.read_csv(outpath))
        ymin = total[0]['year'].max() + 1

    try:
        for year in range(ymin, ymax + 1):
            LOGGER.info("%d ... %d ... %d", ymin, year, ymax)
            if year not in years_uniq:
                continue

            ds_year = ds.sel(event=ds.time.dt.year == year)
            reals_uniq = np.unique(ds_year['realization'].values)
            chunksize = 10
            for chunkstart in range(0, reals_uniq.size, chunksize):
                chunk = reals_uniq[chunkstart:chunkstart + chunksize]
                ds_year_chunk = ds_year.sel(event=np.isin(ds_year['realization'], chunk))

                wind_year = ds_year_chunk[tcwind].values
                real_year = ds_year_chunk['realization'].values
                event_year = ds_year_chunk.event_id.values

                nevents = wind_year.shape[0]
                max_wind = np.zeros((nevents, natids_uniq.size), dtype=np.float64)
                maximum_at(max_wind, natids_inverse.ravel(), wind_year.reshape(nevents, -1))
                nz_event, nz_natid = max_wind.nonzero()
                total.append(pd.DataFrame({
                    'year': year,
                    'realization': real_year[nz_event],
                    'event_id': event_year[nz_event],
                    'genesis_basin': basin,
                    'ISO3': u_coord.country_to_iso(natids_uniq[nz_natid], representation="alpha3"),
                    'v_land_kn': max_wind[nz_event, nz_natid],
                }))
    except KeyboardInterrupt:
        LOGGER.info("Quitting gracefully after SIGINT...")
    finally:
        # write data to disk (unless we quit so early that there is no data yet)
        if len(total) > 0:
            LOGGER.info("Writing result to %s ...", outpath)
            (
                pd.concat(total, ignore_index=True)
                .sort_values(by=['year', 'realization', 'event_id', 'ISO3'])
                .reset_index(drop=True)
                .to_csv(outpath, index=None)
           )


if __name__ == "__main__":
    import argparse

    gcmls = ['HadGEM2-ES', 'MIROC5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
    rcpls = ['rcp26', 'rcp60', 'rcp85']
    subbasinls = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])

    parser = argparse.ArgumentParser(description='Determine max wind per event and country.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('subbasin', choices=subbasinls, metavar="SUBBASIN",
                        help='The (sub-)basin to consider.')
    parser.add_argument('--wind-input', metavar="SUFFIX",
                        choices=["old", "new", "baseline", "cmip6"], default="new",
                        help="Which wind input to use.")
    parser.add_argument('--scenario', metavar="SCENARIO", type=str, default=None,
                        help="Which scenario to use.")
    args = parser.parse_args()

    main(args.gcm, args.rcp, args.subbasin, args.wind_input, args.scenario)
