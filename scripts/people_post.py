
import datetime as dt
import pathlib
import sys

import pandas as pd
import numpy as np

import climada.util.coordinates as u_coord
from climada_petals.hazard.emulator import const

INPUT_DIR = pathlib.Path("output")
INPUT = lambda gcm, rcp, basin, ssp, suffix, minwind: INPUT_DIR.joinpath(
    f"TC-people_affected_by_event_and_country_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_minwind-{minwind}_ssp-{ssp}_{suffix}.csv")

BIAS_DIR = INPUT_DIR / "bias_factors"
BIAS_FILE = lambda gcm, rcp, baseyear, suffix, minwind: BIAS_DIR.joinpath(
    f"TC-bias_factors_by_region_gcm-{gcm}"
    f"_{rcp}_minwind-{minwind}_ssp{baseyear}_{suffix}.csv")

OUTPUT_DIR = INPUT_DIR / "agg_by_ssp"
OUTPUT_COUNTRY = lambda gcm, rcp, ssp, suffix, minwind: OUTPUT_DIR.joinpath(
    f"TC-people_affected_by_country_gcm-{gcm}"
    f"_{rcp}_minwind-{minwind}_ssp{ssp}_{suffix}.csv")
OUTPUT_BASIN = lambda gcm, rcp, ssp, suffix, minwind: OUTPUT_DIR.joinpath(
    f"TC-people_affected_by_basin_gcm-{gcm}"
    f"_{rcp}_minwind-{minwind}_ssp{ssp}_{suffix}.csv")

BASINS = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])
COLS = ["time", "year", "realisation", "basin", "cty_numeric", "affected"]


def load_sim_affected(gcm, rcp, ssp, suffix, minwind):
    result = []
    for basin in BASINS:
        fname = INPUT(gcm, rcp, basin, ssp, suffix, minwind)
        if not fname.exists():
            print(f"File for basin {basin} doesn't exist, assuming that subbasins exist...")
            continue
        df = pd.read_csv(fname)
        df = df[df['affected'] > 0]
        if np.all(df['time'] > 1):
            # realisation is encoded as day of year (old data format)
            df['timedt'] = dt.datetime(1850, 1, 1) + pd.to_timedelta(df['time'], unit='days')
            df['realisation'] = df['timedt'].dt.dayofyear
            df = df.drop('timedt', 1)
        else:
            # 'time' column is 'realisation' (new data format)
            df['realisation'] = df['time']
        df['basin'] = basin
        df = df[COLS]
        result.append(df)
    result = pd.concat(result, ignore_index=True)
    return result.drop(labels=['time'], axis=1)


def main(gcm, rcp, ssp, wind_input, minwind, scenario):
    print(f"Filtering events for gcm={gcm}, rcp={rcp}, ssp={ssp} ...")

    suffix = wind_input if scenario is None else f"{scenario}_{wind_input}"
    out_country = OUTPUT_COUNTRY(gcm, rcp, ssp, suffix, minwind)
    out_basin = OUTPUT_BASIN(gcm, rcp, ssp, suffix, minwind)

    result = load_sim_affected(gcm, rcp, ssp, suffix, minwind)

    # load bias factors
    bias_df = pd.read_csv(BIAS_FILE(gcm, rcp, 2015, suffix, minwind), keep_default_na=False)
    bias_df = bias_df.drop(columns=['factor_wind']).rename(columns={'factor_affected': 'factor'})

    # bias-correction by basin
    result['basin_main'] = result['basin'].str.slice(0, 2)
    result = result.merge(bias_df, left_on=["basin_main"], right_on=["region_id"], how="left")
    result['factor'] = result['factor'].fillna(1)
    result['affected_corr_basin'] = result['affected'] / result['factor']
    result = result.drop(columns=["factor", "basin_main", "region_id"])

    # bias-correction by country
    result['iso_a3'] = u_coord.country_to_iso(
        result['cty_numeric'].values, representation="alpha3", fillvalue="")
    result = result.merge(bias_df, left_on=["iso_a3"], right_on=["region_id"], how="left")
    result['factor'] = result['factor'].fillna(1)
    result['affected_corr_country'] = result['affected'] / result['factor']
    result = result.drop(columns=['factor', 'iso_a3', "region_id"])

    print(f"Writing to {out_basin} ...")
    result_basin = result.drop(labels=['cty_numeric'], axis=1)
    grp = ['year', 'realisation', 'basin']
    result_basin = result_basin.groupby(grp).sum().reset_index(level=grp)
    result_basin.to_csv(out_basin, index=None)

    print(f"Writing to {out_country} ...")
    result_cty = result.drop(labels=['basin'], axis=1)
    grp = ['year', 'realisation', 'cty_numeric']
    result_cty = result_cty.groupby(grp).sum().reset_index(level=grp)
    result_cty.to_csv(out_country, index=None)


if __name__ == "__main__":
    import argparse

    gcmls = ['HadGEM2-ES', 'MIROC5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
    rcpls = ['rcp26', 'rcp60', 'rcp85']
    subbasinls = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])

    parser = argparse.ArgumentParser(description='Aggregate results by basin and by country.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('ssp', type=int, metavar="N",
                        help='Population input: number of SSP or year for frozen population.')
    parser.add_argument('--wind-input', metavar="SUFFIX",
                        choices=["old", "new", "baseline", "cmip6"], default="new",
                        help="Which wind input to use.")
    parser.add_argument('--scenario', metavar="SCENARIO", type=str, default=None,
                        help="Which scenario to use.")
    parser.add_argument('--minwind', type=int, default=64,
                        help=("Minimum wind speed threshold (in knots) "
                              "for people to be 'affected'."))
    args = parser.parse_args()

    main(args.gcm, args.rcp, args.ssp, args.wind_input, args.minwind, args.scenario)
