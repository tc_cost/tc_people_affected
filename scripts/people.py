
import datetime as dt
import logging
import pathlib
import re
import sys
import warnings

import netCDF4 as nc
import scipy.sparse as sp
import numpy as np
import pandas as pd
import rasterio

from climada.hazard import Hazard
from climada_petals.hazard.emulator import const
from climada.entity import Exposures, ImpactFunc, ImpactFuncSet
from climada.engine import Impact
from climada.entity.tag import Tag


LOGGER = logging.getLogger('tce_people_affected')
LOGGER.setLevel(logging.DEBUG)
LOGGER.propagate = False
if LOGGER.hasHandlers():
    for handler in LOGGER.handlers:
        LOGGER.removeHandler(handler)
FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
CONSOLE = logging.StreamHandler(stream=sys.stdout)
CONSOLE.setFormatter(FORMATTER)
LOGGER.addHandler(CONSOLE)


INPUT_DIR = pathlib.Path("input")
OUTPUT_DIR = pathlib.Path("output")

POP_DIR = INPUT_DIR / "pop"
POP_KEYS = ["population", "pop_grid", "popc"]

# when using socioeconomics from ssp-pathways, this file is used
# it is also used for fixed socioeconomics if the year is after 2020 (with SSP1 in that case)
NAT_FILE_SSP = POP_DIR / "natids_6m.nc"
POP_FILE_SSP = lambda ssp: POP_DIR / f"SSP{ssp}_1860-2100_remapcon_360as_yearly.nc4"

# when using fixed socioeconomics (with year 2020 or earlier), HYDE population data is used:
NAT_FILE_HYDE = POP_DIR / "natids_5m.nc"
POP_FILE_HYDE = POP_DIR / "population_2015soc_5arcmin_annual_1901_2020.nc"


WIND_DIR = lambda suffix: INPUT_DIR / f"wind_{suffix}"
WIND_FILE_OLD = lambda gcm, rcp, basin: (
    f"TC-hazard_random_realizations-100_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_baseyear-2015_GMT-time_LTE.nc")
WIND_FILE_NEW = lambda gcm, rcp, basin: f"{gcm}_{rcp}_{basin}_1980_2100_100.nc"
WIND_FILE_SCENARIO = lambda gcm, rcp, basin, scenario: (
    f"{gcm}_{rcp}_{basin}__{scenario}_1980-2100_100.nc"
)
WIND_FILE = lambda gcm, rcp, basin, suffix, scenario: WIND_DIR(suffix).joinpath(
    WIND_FILE_SCENARIO(gcm, rcp, basin, scenario) if scenario is not None
    else WIND_FILE_NEW(gcm, rcp, basin) if suffix != "old"
    else WIND_FILE_OLD(gcm, rcp, basin)
)

# output is a huge CSV file with one row for each event+country combination
OUTPUT = lambda gcm, rcp, basin, ssp, suffix, minwind: OUTPUT_DIR.joinpath(
    f"TC-people_affected_by_event_and_country_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_minwind-{minwind}_ssp-{ssp}_{suffix}.csv")

MINYEAR, MAXYEAR = 1980, 2100



def dim_clusters(dim, min_gap_size=50, min_cluster_size=10):
    """Identify clusters in sorted scalar array

    This function finds "gaps" in a sorted array of floats. For example, in the
    array [2, 3, 4, 107, 108, 109, 110], there is a "gap" after the value 4.
    The function then returns the start and end of the "clusters" that are separated by the gaps.
    Furthermore, the "typical" (minimal) step size is returned.

    Parameters
    ----------
        dim : sorted np.ndarray without duplicates
        min_gap_size : float
            Multiplied with `minstep`, the minimum size of a gap between
            clusters. Default: 50.
        min_cluster_size : int
            Minimum number of elements in each cluster.

    Returns
    -------
        clusters : list of pairs (start, end)
            The start and end index of each cluster, sorted in `dim` starting
            from the cluster that includes dim[0].
        minstep : float
            Minimum distance of consecutive elements in `dim`.
    """
    dim_d = np.diff(dim).round(2)
    dim_du, dim_dinv = np.unique(dim_d, return_inverse=True)
    minstep = dim_du[0]

    # indices of gaps
    dim_di = (dim_du >= min_gap_size * minstep).nonzero()[0]
    dim_splt = list(np.isin(dim_dinv, dim_di).nonzero()[0] + 1)
    starts = [0] + dim_splt
    ends = [i - 1 for i in (dim_splt + [len(dim)])]
    clusters = list(zip(starts, ends))

    # merge clusters from right or left until clusters are large enough
    removed = True
    while removed:
        removed = False
        for i in range(0, len(clusters) - 1):
            start, end = clusters[i]
            if end - start + 1 < min_cluster_size:
                clusters[i + 1] = (start, clusters[i + 1][1])
                clusters.pop(i)
                removed = True
                break
        for i in range(len(clusters) - 1, 0, -1):
            start, end = clusters[i]
            if end - start + 1 < min_cluster_size:
                clusters[i - 1] = (clusters[i - 1][0], end)
                clusters.pop(i)
                removed = True
                break
    return clusters, minstep


def rect_clusters(lat, lon):
    """Rectangular cluster regions in a non-regular grid

    For gridded data with irregular step sizes, this function tries to find rectangular subregions
    where the grid step sizes are regular.

    Parameters
    ----------
        lat, lon : 1d ndarrays of floats
            The grid dimensions with variable step sizes.

    Returns
    -------
        rects : list of tuples (lonmin, latmin, lonmax, latmax)
            Each tuple describes a rectangular region. All grid points are
            contained in one of the rectangles.
    """
    lon = np.sort(lon)
    ranges_lat, dlat = dim_clusters(lat)
    ranges_lon, dlon = dim_clusters(lon)
    rects = []
    for i0, i1 in ranges_lat:
        for j0, j1 in ranges_lon:
            latmin, latmax = lat[i0], lat[i1]
            lonmin, lonmax = lon[j0], lon[j1]
            b = np.array([lonmin, latmin, lonmax, latmax], dtype=np.float64)
            b[[0,1]] -= [dlon, dlat]
            b[[2,3]] += [dlon, dlat]
            rects.append(b)
    return rects


def climada_setup_pipeline(gcm, rcp, basin, ssp, wind_input, minwind, scenario):
    """This initializes all CLIMADA objects (Hazard, Exposure, ImpactFunc) that are needed to
    compute the number of affected people within a fixed basin.

    The returned "pipeline" dict contains not only those objects ("hazards", "exposures",
    "impact_function"), but also the handlers for the NetCDF data sets containing global wind
    field (hazard_ds) and population data ("exposures_ds").

    The CLIMADA objects are empty and need to be filled with data from the NetCDF files before they
    can be passed on to a CLIMADA Impact object for impact calculation.

    Note that a single subbasin can be the union of several rectangular regions that need to be
    handled separately (for performance reasons). Therefore, we have several Hazard and Exposure
    objects in the pipeline, one for each of those rectangular regions.
    """

    # TODO: This impact function can be easily replaced in the future by other socio-economic
    # damage functions, such as the TC damage functions by Eberenz et al. 2021. Of course this
    # would also require matching exposure data (economic assets from LitPop).
    imp_fun = ImpactFunc()
    imp_fun.haz_type = 'TC'
    imp_fun.id = 1
    imp_fun.name = 'TC exposed people'
    imp_fun.intensity_unit = 'kt'
    imp_fun.intensity = np.array([0, minwind, minwind + 0.0001, 200])
    imp_fun.mdd = np.array([0, 0, 1, 1])
    imp_fun.paa = np.array([0, 0, 1, 1])
    imp_fun.tag = Tag()
    imp_fun.tag.description = f"exposed if windspeed >{minwind} knots"
    imp_fun.check()
    impf_set = ImpactFuncSet()
    impf_set.append(imp_fun)

    h_ds = nc.Dataset(WIND_FILE(gcm, rcp, basin, wind_input, scenario))
    if len(ssp) == 1:
        natids = nc.Dataset(NAT_FILE_SSP)['natid']
        pop_file = POP_FILE_SSP(ssp)
    elif int(ssp) > 2020:
        natids = nc.Dataset(NAT_FILE_SSP)['natid']
        pop_file = POP_FILE_SSP("1")
    else:
        natids = nc.Dataset(NAT_FILE_HYDE)['natid']
        pop_file = POP_FILE_HYDE
    e_ds = nc.Dataset(pop_file)

    # Since the subbasin areas are not rectangular but are a union of several rectangles, we need
    # to determine the bounds of those sub-rectangles first:
    bounds = rect_clusters(*[np.array(h_ds[v]) for v in ['lat', 'lon']])
    exposures = []
    hazards = []
    for b in bounds:
        with np.printoptions(precision=3, suppress=True):
            LOGGER.info("Subbasin bounds: %s", b)

        h_lat, h_lon = [np.array(h_ds[v]) for v in ['lat', 'lon']]
        sl_lat = np.where((b[1] <= h_lat) & (h_lat <= b[3]))[0]
        sl_lat = slice(sl_lat[0], sl_lat[-1] + 1)
        sl_lon = np.where((b[0] <= h_lon) & (h_lon <= b[2]))[0]
        sl_lon = slice(sl_lon[0], sl_lon[-1] + 1)
        h_lat, h_lon = h_lat[sl_lat], h_lon[sl_lon]
        LOGGER.info("Windfield data dimensions: (%d, %d)", h_lat.size, h_lon.size)

        haz = Hazard(haz_type='TC')
        haz.units = 'kt'
        haz.tag.description = "precalculated windfield in knots"
        h_lat, h_lon = [a.T.ravel() for a in np.meshgrid(h_lat, h_lon)]
        haz.centroids.set_lat_lon(h_lat, h_lon)
        hazards.append({
            "hazard": haz,
            "ds_slices": [sl_lat, sl_lon],
        })

        e_lat, e_lon = [np.array(e_ds[v]) for v in ['lat', 'lon']]
        sl_lat = np.where((b[1] <= e_lat) & (e_lat <= b[3]))[0]
        sl_lat = slice(sl_lat[0], sl_lat[-1] + 1)
        sl_lon = np.where((b[0] <= e_lon) & (e_lon <= b[2]))[0]
        sl_lon = slice(sl_lon[0], sl_lon[-1] + 1)
        e_lat, e_lon = e_lat[sl_lat], e_lon[sl_lon]
        e_natids = np.array(natids[sl_lat,sl_lon])
        if e_lat[1] < e_lat[0]:
            e_lat = np.flip(e_lat, axis=0)
            e_natids = np.flipud(e_natids)
        e_width, e_height = e_lon.size, e_lat.size
        e_transform = rasterio.transform.from_bounds(*b, e_width, e_height)
        LOGGER.info("Exposure data dimensions: (%d, %d)", e_height, e_width)

        exp = Exposures()
        exp.tag = Tag()
        exp.tag.description = "exposed population"
        exp.value_unit = 'people'
        exp.meta = {
            'width': e_width,
            'height': e_height,
            'transform': e_transform,
            'crs': "EPSG:4326",
        }
        e_lat, e_lon = [a.T.ravel() for a in np.meshgrid(e_lat, e_lon)]
        exp.gdf['longitude'] = e_lon
        exp.gdf['latitude'] = e_lat

        # this is the most non-trivial pre-processing step as it matches grid cells in the hazard
        # grid with grid cells in the exposure grid (the grids might be different!)
        exp.assign_centroids(haz)

        exposures.append({
            "frozen": int(ssp) if len(ssp) > 1 else None,
            "exposure": exp,
            "ds_slices": [sl_lat, sl_lon],
            "natids": e_natids.ravel(),
        })

    pipeline = {
        "hazards": hazards,
        "hazard_ds": h_ds,
        "exposures": exposures,
        "exposures_ds": e_ds,
        "impact_function": impf_set,
    }
    return pipeline


def climada_update_data(pipeline, year):
    """Initialize a "pipeline" (see climada_setup_pipeline) with hazard and exposure data according
    to the given year."""
    LOGGER.info("Update Hazard and Exposure data...")
    h_ds, e_ds = pipeline['hazard_ds'], pipeline['exposures_ds']

    t_curr_year = year_to_days(year)
    t_next_year = year_to_days(year + 1)
    times = np.array(h_ds['time'])
    sl_time = (t_curr_year <= times) & (times < t_next_year)
    sl_time = sl_time.nonzero()[0]
    sl_time = slice(sl_time[0], sl_time[-1] + 1)
    tcwind = h_ds['tcwind' if 'tcwind' in h_ds.variables else 'wind'][sl_time]
    nevents = tcwind.shape[0]

    for haz_dict, exp_dict in zip(pipeline['hazards'], pipeline['exposures']):
        sl_lat, sl_lon = exp_dict['ds_slices']
        exp = exp_dict['exposure']
        exp_year = year if exp_dict['frozen'] is None else exp_dict['frozen']
        pop_key = [key for key in POP_KEYS if key in e_ds.variables][0]
        pop_year_min = int(re.match(r"years since ([0-9]{4}).*", e_ds['time'].units).group(1))
        value = e_ds[pop_key][exp_year - pop_year_min]
        value = np.array(value[sl_lat, sl_lon])
        if e_ds['lat'][1] < e_ds['lat'][0]:
            value = np.flipud(value)
        value = value.reshape(-1)
        value[value < 0] = 0
        exp.gdf['value'] = value
        exp.gdf['impf_TC'] = np.ones(value.size)

        sl_lat, sl_lon = haz_dict['ds_slices']
        haz = haz_dict['hazard']
        haz.intensity = sp.csr_matrix(tcwind[:,sl_lat,sl_lon].reshape(nevents, -1))
        haz.fraction = haz.intensity.copy()
        haz.fraction[haz.fraction != 0.] = 1.
        haz.event_id = np.arange(nevents)
        haz.event_name = [f'TC{i}' for i in haz.event_id]
        # frequency is NOT used, since we only take the `imp_mat` output!
        haz.frequency = np.ones(nevents)/nevents
        haz.orig = np.ones(nevents, dtype=bool)
        if "realization" in h_ds.variables:
            # new wind input has explicit realization variable
            haz.date = np.array(h_ds['realization'])[sl_time]
        else:
            # old wind input stores realization as day of year
            haz.date = times[sl_time]


def climada_compute_impact_by_country(pipeline):
    """From a given "pipeline" (see climada_setup_pipeline) that is already initialized with a
    call to climada_update_data, compute the number of people within each country that are affected
    by each event in the data."""
    LOGGER.info("Compute impact...")
    natids_all = np.concatenate([e['natids'] for e in pipeline['exposures']])
    natids_uniq = np.unique(natids_all)
    impf_set = pipeline['impact_function']
    result = []
    for haz_dict, exp_dict in zip(pipeline['hazards'], pipeline['exposures']):
        haz = haz_dict['hazard']
        exp = exp_dict['exposure']
        natids = exp_dict['natids']

        # compute the number of affected people within each grid cell:
        imp = Impact()
        imp.calc(exp, impf_set, haz, save_mat=True)

        # aggregate the numbers of affected people by country:
        subresult = []
        for cty_id in natids_uniq:
            cty_cents = (natids == cty_id).nonzero()[0]
            affected = imp.imp_mat[:,cty_cents].sum(axis=1)
            subresult.append(pd.DataFrame({
                "time": haz.date,
                "cty_numeric": cty_id,
                "affected": np.array(affected).ravel(),
            }))

        result.append(pd.concat(subresult, ignore_index=True))
    total = result[0]
    total['affected'] = sum(r['affected'] for r in result)
    # This will contain a lot of 0 affected people and could be compressed to
    # only affected countries, but then storm events with 0 affected people
    # are not recognizable in the data
    return total


def days_to_datetime(days):
    """Get a datetime object from the number of days since 1850-01-01"""
    return dt.datetime(1850, 1, 1) + dt.timedelta(days)


def year_to_days(year):
    """Convert a year (like 1993) to the number of days since 1850-01-01"""
    return (dt.datetime(year, 1, 1) - dt.datetime(1850, 1, 1)).days


def main(gcm, rcp, subbasin, ssp, wind_input, minwind, scenario):
    """Set up a "pipeline" of CLIMADA objects and apply the impact calculation year-by-year"""

    pipeline = climada_setup_pipeline(gcm, rcp, subbasin, ssp, wind_input, minwind, scenario)

    suffix = wind_input if scenario is None else f"{scenario}_{wind_input}"
    outfile = OUTPUT(gcm, rcp, subbasin, ssp, suffix, minwind)
    total = []

    ymin, ymax = MINYEAR, MAXYEAR
    if outfile.exists():
        LOGGER.info("Load %s ...", outfile)
        total.append(pd.read_csv(outfile))
        ymin = total[0]['year'].max() + 1

    try:
        for year in range(ymin, ymax + 1):
            LOGGER.info("%d ... %d ... %d", ymin, year, ymax)
            # For performance reasons (memory requirements), we process each year separately.
            # When the process is interrupted prematurely, data is written to disk so that the
            # script can continue later at that point.

            # TODO: In theory, this could be parallelized (using OpenMP). However, we would need
            # to use separate pipelines for each parallel process.

            climada_update_data(pipeline, year)
            affected = climada_compute_impact_by_country(pipeline)
            affected['year'] = year

            LOGGER.info("Append to total...")
            total.append(affected)
    except KeyboardInterrupt:
        LOGGER.info("Quitting gracefully after SIGINT...")
    finally:
        # write data to disk (unless we quit so early that there is no data yet)
        if len(total) > 0:
            LOGGER.info("Save result to %s ...", outfile)
            pd.concat(total, ignore_index=True).to_csv(outfile, index=None)


if __name__ == "__main__":
    import argparse

    gcmls = ['HadGEM2-ES', 'MIROC5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
    rcpls = ['rcp26', 'rcp60', 'rcp85']
    subbasinls = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])

    parser = argparse.ArgumentParser(description='Determine number of affected people.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('subbasin', choices=subbasinls, metavar="SUBBASIN",
                        help='The (sub-)basin to consider.')
    parser.add_argument('ssp', metavar="N",
                        help='Population input: number of SSP or year for frozen population.')
    parser.add_argument('--wind-input', metavar="SUFFIX",
                        choices=["old", "new", "baseline", "cmip6"], default="new",
                        help="Which wind input to use.")
    parser.add_argument('--scenario', metavar="SCENARIO", type=str, default=None,
                        help="Which scenario to use.")
    parser.add_argument('--minwind', metavar="KNOTS", type=int, default=64,
                        help=("Minimum wind speed threshold (in knots) "
                              "for people to be 'affected'."))
    args = parser.parse_args()

    main(args.gcm, args.rcp, args.subbasin, args.ssp, args.wind_input, args.minwind, args.scenario)
