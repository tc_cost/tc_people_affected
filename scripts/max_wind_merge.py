
import logging
import pathlib
import sys

import pandas as pd

from climada_petals.hazard.emulator import const


LOGGER = logging.getLogger('tce_people_affected')
LOGGER.setLevel(logging.DEBUG)
LOGGER.propagate = False
if LOGGER.hasHandlers():
    for handler in LOGGER.handlers:
        LOGGER.removeHandler(handler)
FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
CONSOLE = logging.StreamHandler(stream=sys.stdout)
CONSOLE.setFormatter(FORMATTER)
LOGGER.addHandler(CONSOLE)

OUTPUT_DIR = pathlib.Path("output")

INPUT = lambda gcm, rcp, basin, suffix: OUTPUT_DIR.joinpath(
    f"TC-max_wind_by_event_and_country_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_{suffix}.csv")

OUTPUT = lambda gcm, rcp, suffix: OUTPUT_DIR.joinpath(
    f"TC-max_wind_by_event_and_country_gcm-{gcm}_{rcp}_{suffix}.csv")


def main(gcm, rcp, wind_input, scenario):
    suffix = wind_input if scenario is None else f"{scenario}_{wind_input}"
    outpath = OUTPUT(gcm, rcp, suffix)
    if outpath.exists():
        LOGGER.info("File exists: %s", outpath)
        sys.exit()

    subbasinls = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])
    inpaths = [INPUT(gcm, rcp, basin, suffix) for basin in subbasinls]
    inpaths = [path for path in inpaths if path.exists()]
    total = [pd.read_csv(fname, na_filter=False) for fname in inpaths]

    df = pd.concat(total, ignore_index=True)
    df['event_id'] = df['genesis_basin'] + df['event_id'].astype(str).str.zfill(7)
    df = df.sort_values(by=['year', 'realization', 'event_id', 'ISO3']).reset_index(drop=True)

    LOGGER.info("Writing result to %s ...", outpath)
    df.to_csv(outpath, index=None)

    for path in inpaths:
        path.unlink()


if __name__ == "__main__":
    import argparse

    gcmls = ['HadGEM2-ES', 'MIROC5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
    rcpls = ['rcp26', 'rcp60', 'rcp85']

    parser = argparse.ArgumentParser(description='Merge max-wind-results into one file.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('--wind-input', metavar="SUFFIX",
                        choices=["old", "new", "baseline", "cmip6"], default="new",
                        help="Which wind input to use.")
    parser.add_argument('--scenario', metavar="SCENARIO", type=str, default=None,
                        help="Which scenario to use.")
    args = parser.parse_args()

    main(args.gcm, args.rcp, args.wind_input, args.scenario)
