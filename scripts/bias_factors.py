
import datetime as dt
import pathlib
import sys

import pandas as pd
import numpy as np

import climada.util.coordinates as u_coord
from climada_petals.hazard.emulator import const

INPUT_DIR = pathlib.Path("output")
INPUT_PEOPLE_FNAME = lambda gcm, rcp, basin, ssp, suffix, minwind: (
    f"TC-people_affected_by_event_and_country_gcm-{gcm}_{rcp}"
    f"_basin-{basin}_minwind-{minwind}_ssp-{ssp}_{suffix}.csv"
)
INPUT_MAXWIND_FNAME = lambda gcm, rcp, suffix: (
    f"TC-max_wind_by_event_and_country_gcm-{gcm}_{rcp}_{suffix}.csv"
)

OUTPUT_DIR = INPUT_DIR / "bias_factors"
OUTPUT = lambda gcm, rcp, ssp, suffix, minwind: OUTPUT_DIR.joinpath(
    f"TC-bias_factors_by_region_gcm-{gcm}"
    f"_{rcp}_minwind-{minwind}_ssp{ssp}_{suffix}.csv")

BASINS = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])
COLS = ["time", "year", "realisation", "basin", "cty_numeric", "affected"]
PERIOD = (1980, 2015)

tce_dat_path = "/p/projects/ebm/TCs/TCE-DAT/modified_TCE-DAT_{baseyear}-exposure_1950-2015.csv"


def load_tce_dat(baseyear, minwind):
    exp_df = pd.read_csv(
        tce_dat_path.format(baseyear=baseyear),
        keep_default_na=False, comment="#")
    exp_df = exp_df[[
        'year', 'IBTrACS_ID', 'genesis_basin', 'ISO3',
        f'{minwind}kn_pop', 'v_land_kn']]
    exp_df.columns = ['year', 'event_id', 'basin', 'iso_a3', 'affected', 'v_land_kn']
    exp_df = exp_df[(exp_df['year'] >= PERIOD[0]) & (exp_df['year'] <= PERIOD[1])]
    return exp_df


def load_sim_affected(gcm, rcp, baseyear, suffix, minwind):
    result = []
    for basin in BASINS:
        fname = INPUT_DIR / INPUT_PEOPLE_FNAME(gcm, rcp, basin, baseyear, suffix, minwind)
        if not fname.exists():
            print(f"File for basin {basin} doesn't exist, assuming that subbasins exist...")
            continue
        df = pd.read_csv(fname)
        df = df[df['affected'] > 0]
        if np.all(df['time'] > 1):
            # realisation is encoded as day of year (old data format)
            df['timedt'] = dt.datetime(1850, 1, 1) + pd.to_timedelta(df['time'], unit='days')
            df['realisation'] = df['timedt'].dt.dayofyear
            df = df.drop('timedt', 1)
        else:
            # 'time' column is 'realisation' (new data format)
            df['realisation'] = df['time']
        df['basin'] = basin
        df = df[COLS]
        result.append(df)
    result = pd.concat(result, ignore_index=True)
    result['iso_a3'] = u_coord.country_to_iso(
        result['cty_numeric'].values, representation="alpha3", fillvalue="")
    result['basin'] = result['basin'].str.slice(0, 2)
    result = result.drop(columns=['time', 'cty_numeric'])
    result = result[(result['year'] >= PERIOD[0]) & (result['year'] <= PERIOD[1])]
    result = result[result['affected'] > 0]
    return result


def load_sim_maxwind(gcm, rcp, suffix):
    path = INPUT_DIR / INPUT_MAXWIND_FNAME(gcm, rcp, suffix)
    result = pd.read_csv(path, keep_default_na=False).rename(
        columns={"ISO3": "iso_a3",
                 "genesis_basin": "basin",
                 "realization": "realisation"})
    result['basin'] = result['basin'].str.slice(0, 2)
    result = result[(result['year'] >= PERIOD[0]) & (result['year'] <= PERIOD[1])]
    return result


def exp_aggregate_sum(exp_df):
    # aggregate by year (and realisation)
    groupby = ['year']
    if 'realisation' in exp_df.columns:
        groupby.append("realisation")
    return exp_df.groupby(groupby).sum().reset_index()


def exp_mean_max_per_event(exp_df, by, variable):
    exp_df = exp_df.drop(columns=["iso_a3" if by == 'basin' else 'basin'])
    cols = list(exp_df.columns)
    cols.remove(variable)
    exp_df = exp_df.groupby(cols).max().reset_index()
    # aggregate by year (and realisation)
    groupby = ['year']
    if 'realisation' in exp_df.columns:
        groupby.append("realisation")
    return exp_df.groupby(groupby).mean().reset_index()


def main(gcm, rcp, baseyear, wind_input, minwind, scenario):
    print(f"Determine bias factors for gcm={gcm}, rcp={rcp}, baseyear={baseyear} ...")

    suffix = wind_input if scenario is None else f"{scenario}_{wind_input}"
    outfile = OUTPUT(gcm, rcp, baseyear, suffix, minwind)
    sim_aff = load_sim_affected(gcm, rcp, baseyear, suffix, minwind)
    sim_wind = load_sim_maxwind(gcm, rcp, suffix)

    tce_dat_df = load_tce_dat(baseyear, minwind)
    obs_aff = tce_dat_df[tce_dat_df['affected'] > 0]
    obs_wind = tce_dat_df.drop(columns=['affected'])

    bias_factors = []

    for basin in np.unique(tce_dat_df['basin']):
        if basin == "SA":
            continue
        obs_df = exp_aggregate_sum(obs_aff[obs_aff['basin'] == basin])
        sim_df = exp_aggregate_sum(sim_aff[sim_aff['basin'] == basin])
        bias_affected = sim_df['affected'].mean() / obs_df['affected'].mean()

        obs_df = exp_mean_max_per_event(obs_wind[obs_wind['basin'] == basin], "basin", "v_land_kn")
        sim_df = exp_mean_max_per_event(sim_wind[sim_wind['basin'] == basin], "basin", "v_land_kn")
        bias_wind = sim_df['v_land_kn'].mean() / obs_df['v_land_kn'].mean()

        bias_factors.append((basin, bias_affected, bias_wind))

    for country in np.unique(tce_dat_df['iso_a3']):
        obs_df = exp_aggregate_sum(obs_aff[obs_aff['iso_a3'] == country])
        sim_df = exp_aggregate_sum(sim_aff[sim_aff['iso_a3'] == country])
        if obs_df.shape[0] <= 5 or sim_df.shape[0] == 0:
            continue
        bias_affected = sim_df['affected'].mean() / obs_df['affected'].mean()

        obs_df = exp_mean_max_per_event(obs_wind[obs_wind['iso_a3'] == country], "country", "v_land_kn")
        sim_df = exp_mean_max_per_event(sim_wind[sim_wind['iso_a3'] == country], "country", "v_land_kn")
        bias_wind = sim_df['v_land_kn'].mean() / obs_df['v_land_kn'].mean()

        bias_factors.append((country, bias_affected, bias_wind))

    print(f"Writing to {outfile} ...")
    (
        pd.DataFrame(bias_factors, columns=["region_id", "factor_affected", "factor_wind"])
        .to_csv(outfile, index=None)
    )


if __name__ == "__main__":
    import argparse

    gcmls = ['HadGEM2-ES', 'MIROC5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
    rcpls = ['rcp26', 'rcp60', 'rcp85']
    subbasinls = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])

    parser = argparse.ArgumentParser(description='Determine biases in computed affected people and max. winds.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('--baseyear', type=int, metavar="YEAR", default=2015,
                        help='Year of frozen socioeconomics.')
    parser.add_argument('--wind-input', metavar="SUFFIX",
                        choices=["old", "new", "baseline", "cmip6"], default="new",
                        help="Which wind input to use.")
    parser.add_argument('--scenario', metavar="SCENARIO", type=str, default=None,
                        help="Which scenario to use.")
    parser.add_argument('--minwind', type=int, default=64,
                        help=("Minimum wind speed threshold (in knots) "
                              "for people to be 'affected'."))
    args = parser.parse_args()

    main(args.gcm, args.rcp, args.baseyear, args.wind_input, args.minwind, args.scenario)
