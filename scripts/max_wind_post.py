
import logging
import pathlib
import sys

import pandas as pd

LOGGER = logging.getLogger('tce_people_affected')
LOGGER.setLevel(logging.DEBUG)
LOGGER.propagate = False
if LOGGER.hasHandlers():
    for handler in LOGGER.handlers:
        LOGGER.removeHandler(handler)
FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
CONSOLE = logging.StreamHandler(stream=sys.stdout)
CONSOLE.setFormatter(FORMATTER)
LOGGER.addHandler(CONSOLE)

OUTPUT_DIR = pathlib.Path("output")

BIAS_DIR = OUTPUT_DIR / "bias_factors"
BIAS_FILE = lambda gcm, rcp, baseyear, suffix, minwind: BIAS_DIR.joinpath(
    f"TC-bias_factors_by_region_gcm-{gcm}"
    f"_{rcp}_minwind-{minwind}_ssp{baseyear}_{suffix}.csv")

OUTPUT = lambda gcm, rcp, suffix: OUTPUT_DIR.joinpath(
    f"TC-max_wind_by_event_and_country_gcm-{gcm}_{rcp}_{suffix}.csv")

def main(gcm, rcp, wind_input, scenario):
    suffix = wind_input if scenario is None else f"{scenario}_{wind_input}"
    outpath = OUTPUT(gcm, rcp, suffix)
    df = pd.read_csv(outpath, keep_default_na=False)

    # load bias factors
    bias_df = pd.read_csv(BIAS_FILE(gcm, rcp, 2015, suffix, 34), keep_default_na=False)
    bias_df = bias_df.drop(columns=['factor_affected']).rename(columns={'factor_wind': 'factor'})

    # bias-correction by basin
    df['basin_main'] = df['genesis_basin'].str.slice(0, 2)
    df = df.merge(bias_df, left_on=["basin_main"], right_on=["region_id"], how="left")
    df['factor'] = df['factor'].fillna(1)
    df['v_land_kn_corr_basin'] = df['v_land_kn'] / df['factor']
    df = df.drop(columns=["factor", "basin_main", "region_id"])

    # bias-correction by country
    df = df.merge(bias_df, left_on=["ISO3"], right_on=["region_id"], how="left")
    df['factor'] = df['factor'].fillna(1)
    df['v_land_kn_corr_country'] = df['v_land_kn'] / df['factor']
    df = df.drop(columns=['factor', "region_id"])

    LOGGER.info("Writing result to %s ...", outpath)
    df.to_csv(outpath, index=None)

if __name__ == "__main__":
    import argparse

    gcmls = ['HadGEM2-ES', 'MIROC5', 'GFDL-ESM2M', 'IPSL-CM5A-LR']
    rcpls = ['rcp26', 'rcp60', 'rcp85']

    parser = argparse.ArgumentParser(description='Combine max-wind-results into one file.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('--wind-input', metavar="SUFFIX",
                        choices=["old", "new", "baseline", "cmip6"], default="new",
                        help="Which wind input to use.")
    parser.add_argument('--scenario', metavar="SCENARIO", type=str, default=None,
                        help="Which scenario to use.")
    args = parser.parse_args()

    main(args.gcm, args.rcp, args.wind_input, args.scenario)
