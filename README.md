
# Number of Affected People from TC Emulator Outputs

Compute numbers of affected people from wind field outputs of the tropical cyclone emulator.

The scripts in this repository use the NetCDF wind field outputs from
the [TC Emulator Scripts](https://gitlab.pik-potsdam.de/tovogt/tc_emulator) to determine the number of people that
are affected by TC winds - either with population evolving according to SSP scenarios or with population fixed to
a baseline year.

## Installation

The scripts are tested on Ubuntu 20.04 and on the PIK cluster (with SLURM), but should run on any
platform (e.g, Windows, macOS) that is supported by the CLIMADA Python package.

Install the Python project CLIMADA (including climada_petals) in a conda environment as specified
in the [CLIMADA documentation](https://climada-python.readthedocs.io/en/stable/guide/Guide_Installation.html).
All scripts contained in this repository can be executed within that environment.

## Execution

The main script that does most of the work is `scripts/people.py`. It needs 6 input parameters on the command line:
`GCM RCP SUBBASIN SSP_N --wind-input SUFFIX --minwind KNOTS`. For example:
```shell
python scripts/people.py HadGEM2-ES rcp60 NA 2 --wind-input baseline --minwind 48
```
Use `scripts/people.py --help` in order to obtain a description of all the parameters.

A much simpler script in this repository is `scripts/max_wind.py` that determines the maximum wind speeds attained by
each storm in each country of the world. It needs 4 input parameters: `GCM RCP SUBBASIN --wind-input SUFFIX`.
After calling `scripts/max_wind.py` for all subbasins of one GCM-RCP-SUFFIX combination, the script
`scripts/max_wind_merge.py` needs to be called. It does not change the data, but only concatenates all the outputs into
a single CSV file.

While the emulator already removes biases in the original TC track sets, there might still be biases in the number of
affected people when comparing them with observational data in the historical period. Therefore, there is a script to
quantify these biases: `scripts/bias_factors.py`. Bias factors are computed on a country and on a basin-level
separately, and biases are also determined for the maximum wind speeds.

For convenience, the resulting people counts should be aggregated by year and region (basin or country).
The post-processing script `scripts/people_post.py`  will create CSV files containing one row for each
basin+year (or country+year) with the total number of people affected in that basin (or country) and year. There will
be columns for the bare numbers, and for the numbers corrected according to the precomputed bias factors.

The script `scripts/max_wind_post.py` does not aggregate by region, but only adds columns with the bias-corrected
wind speeds to the original CSV files. After that, each row will still contain information about a single TC event.

## Input/Output Data

Necessary input data (TC windfields, population) will be loaded from `./input/` while computed people counts
are stored in CSV files in `./output/`:
```
./
├── input
│   ├── wind_old
│   │   └── *.nc
│   ├── wind_new
│   │   └── *.nc
│   ├── wind_baseline
│   │   └── *.nc
│   ├── wind_cmip6
│   │   └── *.nc
│   └── pop
│       └── *.nc
└── output
    ├── agg_by_ssp
    │   └── *.csv
    └── *.csv
```

## Parallelization

The TC emulator outputs are for a separate regions (ocean basins). Computation of the number of affected people are
independent from each other for different regions, and can be easily parallelized (also for different GCMs and RCPs).
The max. wind aggregation script can also be run in parallel to the people counting scripts.

However, note that the script `bias_factors.py` that determines the bias factors needs to be run *after* `people.py`
and `max_wind.py` (and `max_wind_merge.py`) and *before* running the postprocessing scripts `people_post.py` and
`max_wind_post.py`:

```
people.py BASIN_1 ──>─┐
people.py BASIN_2 ──>─┤
⋮      ⋮      ⋮       ├─────────────>─────────────┐
people.py BASIN_N ──>─┘                           │                       ┌─>─ people_post.py
                                                  ├─>─ bias_factors.py ─>─┤
max_wind.py BASIN_1 ──>─┐                         │                       └─>─ max_wind_post.py
max_wind.py BASIN_2 ──>─┤                         │
⋮      ⋮      ⋮         ├─>─ max_wind_merge.py ─>─┘
max_wind.py BASIN_N ──>─┘
```

In the folder `slurm`, you find an example of how to run these scripts on a SLURM cluster, organized as job arrays that
get their input arguments from the lines in given text files.
